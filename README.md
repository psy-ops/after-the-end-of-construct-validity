# After the End of Construct Validity

### After the End of Construct Validity: Narrative Response Models as a Conceptual Tool to Facilitate Epistemic Iteration

The rendered article is available at https://psy-ops.gitlab.io/after-the-end-of-construct-validity as a website and at https://psy-ops.gitlab.io/after-the-end-of-construct-validity/peters--gruijters---2022---after-the-end-of-construct-validity--narrative-response-models-and-epistemic-iteration---manuscript.pdf as a PDF document.

<!-- Note that the preprint is located at https://doi.org/10.31234/osf.io/8tpcv. -->

